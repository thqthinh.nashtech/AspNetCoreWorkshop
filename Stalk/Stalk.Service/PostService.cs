﻿namespace Stalk.Service
{
    using Stalk.Model;
    using Stalk.Repository;
    using System;
    using System.Collections.Generic;
    using System.Text;

    public interface IPostService
    {
        int GetTotalNumberOfPosts();
        int GetTotalNumberOfPostsByCategory(string category);
        ModelPost GetById(int id);
        IEnumerable<ModelPost> GetTop(int top);
        IEnumerable<ModelPost> GetAll(bool includeUnpublished);
        IEnumerable<ModelPost> GetAllPaged(int count, int page = 1, bool includeUnpublished = false);
        IEnumerable<ModelPost> GetQueryPaged(string query, int count, int page = 1);
        int GetQueryPagedCount(string query);
        IEnumerable<ModelPost> GetAllByCategoryPaged(string category, int count, int page = 1);
        void Remove(string url);
    }

    public class PostService : IPostService
    {
        private readonly IPostRepository postRepository;

        public PostService(IPostRepository postRepository)
        {
            this.postRepository = postRepository;
        }

        public IEnumerable<ModelPost> GetAll(bool includeUnpublished)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ModelPost> GetAllByCategoryPaged(string category, int count, int page = 1)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ModelPost> GetAllPaged(int count, int page = 1, bool includeUnpublished = false)
        {
            throw new NotImplementedException();
        }

        public ModelPost GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ModelPost> GetQueryPaged(string query, int count, int page = 1)
        {
            throw new NotImplementedException();
        }

        public int GetQueryPagedCount(string query)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ModelPost> GetTop(int top)
        {
            throw new NotImplementedException();
        }

        public int GetTotalNumberOfPosts()
        {
            throw new NotImplementedException();
        }

        public int GetTotalNumberOfPostsByCategory(string category)
        {
            throw new NotImplementedException();
        }

        public void Remove(string url)
        {
            throw new NotImplementedException();
        }
    }
}
