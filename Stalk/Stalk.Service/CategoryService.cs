﻿namespace Stalk.Service
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using AutoMapper;
    using Stalk.Core;
    using Stalk.Model;
    using Stalk.Repository;

    public interface ICategoryService
    {
        IEnumerable<ModelCategory> GetAll();
        IEnumerable<ModelCategory> GetAllPaged(int count, int page);
        ModelCategory GetById(int id);
        ModelCategory Add(ModelCategory category);
        void Update(ModelCategory item);
        void Remove(int id);
    }

    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository categoryRepository;

        public CategoryService(ICategoryRepository categoryRepository)
        {
            this.categoryRepository = categoryRepository;
        }

        public ModelCategory Add(ModelCategory category)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get all Categories
        /// </summary>
        /// <returns>Collection of Categories</returns>
        public IEnumerable<ModelCategory> GetAll()
        {
            return categoryRepository.GetAll()
                    .Select(Mapper.Map<Category, ModelCategory>)
                    .ToList();
        }

        public IEnumerable<ModelCategory> GetAllPaged(int count, int page)
        {
            throw new NotImplementedException();
        }

        public ModelCategory GetById(int id)
        {
            throw new NotImplementedException();
        }

        public void Remove(int id)
        {
            throw new NotImplementedException();
        }

        public void Update(ModelCategory item)
        {
            throw new NotImplementedException();
        }

        IEnumerable<ModelCategory> ICategoryService.GetAll()
        {
            throw new NotImplementedException();
        }
    }
}
