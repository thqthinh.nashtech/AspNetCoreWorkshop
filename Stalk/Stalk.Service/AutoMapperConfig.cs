﻿using AutoMapper;
using Stalk.Core;
using Stalk.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Stalk.Service
{
    public static class AutoMapperConfig
    {
        public static void Config()
        {
            Mapper.Initialize(cfg => {    
                cfg.CreateMap<ModelCategory, Category>().ReverseMap();
                cfg.CreateMap<ModelPost, Post>().ReverseMap();
            });
        }
    }
}
