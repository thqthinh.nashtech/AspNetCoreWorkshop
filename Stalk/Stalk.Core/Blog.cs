﻿namespace Stalk.Core
{
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Blog")]
    public class Blog
    {
        public int BlogId { get; set; }

        public string BlogTitle { get; set; }

        public virtual ICollection<Post> Posts { get; set; }
    }
}
