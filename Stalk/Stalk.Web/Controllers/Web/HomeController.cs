﻿namespace Stalk.Web.Controllers.Web
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Stalk.Service;

    public class HomeController : Controller
    {

        private readonly IPostService postService;

        public HomeController(IPostService postService)
        {
            this.postService = postService;
            
        }

        public IActionResult Index()
        {
            ViewBag.LatestPosts = postService.GetTop(8);

            return View();
        }
    }
}