﻿using Stalk.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Stalk.Repository
{
    public interface ICategoryRepository
    {
        IEnumerable<Category> GetAll();
        IEnumerable<Category> GetAllPaged(int count, int page);
        Category GetById(int id);
    }

    public class CategoryRepository : ICategoryRepository
    {
        private readonly BlogDbContext blogContext;

        public CategoryRepository(BlogDbContext blogContext)
        {
            this.blogContext = blogContext;
        }

        // <summary>
        /// Get all Categories
        /// </summary>
        /// <returns>Category Collection</returns>
        public IEnumerable<Category> GetAll()
        {
            return blogContext.Categories.ToList();
        }

        public IEnumerable<Category> GetAllPaged(int count, int page)
        {
            return blogContext.Categories.Skip((page - 1) * count)
                    .Take(count)
                    .ToList();
        }

        public Category GetById(int id)
        {
            return blogContext.Categories.FirstOrDefault(x => x.CategoryId == id);
        }
    }
}
