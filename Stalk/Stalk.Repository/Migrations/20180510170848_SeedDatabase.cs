﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace Stalk.Repository.Migrations
{
    public partial class SeedDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO Category (Name) VALUES ('Education')");
            migrationBuilder.Sql("INSERT INTO Category (Name) VALUES ('Personal')");
            migrationBuilder.Sql("INSERT INTO Category (Name) VALUES ('Sport')");
            migrationBuilder.Sql("INSERT INTO Category (Name) VALUES ('Movie')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}
