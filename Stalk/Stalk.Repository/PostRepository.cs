﻿using Stalk.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace Stalk.Repository
{
    public interface IPostRepository
    {
        int GetTotalNumberOfPosts();
        int GetTotalNumberOfPostsByCategory(string category);
        Post GetById(int id);
        IEnumerable<Post> GetTop(int top);
        IEnumerable<Post> GetAll(bool includeUnpublished);
        IEnumerable<Post> GetAllPaged(int count, int page = 1, bool includeUnpublished = false);
        IEnumerable<Post> GetQueryPaged(string query, int count, int page = 1);
        int GetQueryPagedCount(string query);
        IEnumerable<Post> GetAllByCategoryPaged(string category, int count, int page = 1);
        void Remove(string url);
    }

    public class PostRepository : IPostRepository
    {
        private readonly BlogDbContext blogContext;

        public PostRepository(BlogDbContext blogContext)
        {
            this.blogContext = blogContext;
        }

        public IEnumerable<Post> GetAll(bool includeUnpublished)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Post> GetAllByCategoryPaged(string category, int count, int page = 1)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Post> GetAllPaged(int count, int page = 1, bool includeUnpublished = false)
        {
            throw new NotImplementedException();
        }

        public Post GetById(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Post> GetQueryPaged(string query, int count, int page = 1)
        {
            throw new NotImplementedException();
        }

        public int GetQueryPagedCount(string query)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Post> GetTop(int top)
        {
            throw new NotImplementedException();
        }

        public int GetTotalNumberOfPosts()
        {
            throw new NotImplementedException();
        }

        public int GetTotalNumberOfPostsByCategory(string category)
        {
            throw new NotImplementedException();
        }

        public void Remove(string url)
        {
            throw new NotImplementedException();
        }
    }
}
