﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stalk.Model
{
    public class ModelPost
    {
        public ModelPost()
        {
            ModelCategory = new ModelCategory();
        }

        public int PostId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Url { get; set; }
        public string Link { get; set; }
        public string Meta { get; set; }
        public string Image { get; set; }
        public string SmallImage { get; set; }
        public string IconImage { get; set; }
        public string Notes { get; set; }
        public DateTime PostedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool Published { get; set; }
        public ModelCategory ModelCategory { get; set; }
    }
}
