﻿using System;

namespace Stalk.Model
{
    public class ModelCategory
    {
        public int CategoryId { get; set; }

        public string Name { get; set; }

        public string Url { get; set; }
    }
}
